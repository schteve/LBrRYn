LBrRYn
======

LBrRYn is an intuitive command line interface for [LBRY\*](https://lbry.io/).

Why
---

The LBRY\* [GUI](https://github.com/lbryio/lbry-app/releases), the "lbry-app", is packaged as a `.deb`, not installable on [Solus\*](https://solus-project.com/home/) (UPDATE: Now in [stable](https://dev.solus-project.com/source/lbry-app/)), and compiling an election-app from source did not seem worthwhile.  I next looked at the [CLI](https://lbryio.github.io/lbry/cli/) but found it cumbersome.  For example the following command lists *all* metadata in a human-unfriendly format: 
   
    lbrynet-cli claim_list_by_channel @[Channel] --page=1 --page_size=10 

I thought there should be an easier way so I started a BASH script, which became the first iteration of LBrRYn (see ./deprecated/), whose features satisfied the basic requirements of the consumer of content but not the publisher.

Given developments in `lbrynet-daemon` after v18.\* that broke some of the BASH script's logic, it became clear a rewrite was in order (see TODO.md).

Requirements
------------

LBrRYn will expect the `lbrynet-daemon` [script](https://lbry.io/quickstart/all) to be in your `PATH`.  If your distribution packages the lbry-app you might like to install it for easy updates of this script  because, at least on Solus, it can be found at `/usr/share/lbry-app/resources/static/daemon/lbrynet-daemon`.  Moreover, you might like to take advantage of LBRY's in-app [rewards](https://lbry.io/faq/earn-credits).

Support
------
If you would like to support LBrRYn, you can send me `LBC` via `bTgYUMnmSufazfb1u2pRugDPminDmtNWE6`:

    LBRY w -s 1.0 bTgYUMnmSufazfb1u2pRugDPminDmtNWE6

Usage
----
 
Disclaimer
---------

\*Some names may be claimed as the property of others.
