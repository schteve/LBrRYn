LBrRYn
======

LBrRYn is an intuitive command line interface for [LBRY\*](https://lbry.io/).

Why
---

The LBRY\* [GUI](https://github.com/lbryio/lbry-app/releases), the "lbry-app", is packaged as a `.deb`, not installable on [Solus\*](https://solus-project.com/home/) (UPDATE: Now in [stable](https://dev.solus-project.com/source/lbry-app/)), and compiling an election-app from source did not seem worthwhile.  I next looked at the [CLI](https://lbryio.github.io/lbry/cli/) but found it cumbersome.  For example the following command lists *all* metadata in a human-unfriendly format: 
   
    lbrynet-cli claim_list_by_channel @[Channel] --page=1 --page_size=10 

I thought there should be an easier way so I started a BASH script, which became LBrRYn.  At the moment, the features satisfy the basic requirements of the consumer of content but not the publisher; one can: 
- play, delete, and list downloaded files 
- subscribe to channels, list their 'claims' and download them
- perform basic management of a wallet
- tip claims, and send LBC to addresses
- get the current settings and set a subset of them 
- start/query the status of/stop the daemon 

Requirements
------------

\*\***N.B.  The script requires lbrynet-daemon/-cli <=v18.0, see TODO**\*\*
The script expects the `lbrynet-daemon` and `lbrynet-cli` [scripts](https://lbry.io/quickstart/all) to be in your `PATH`.  If your distribution packages the lbry-app you might like to install it for easy updates of these scripts  because, at least on Solus, they can be found at, for example, `/usr/share/lbry-app/resources/static/daemon/lbrynet-cli`.  Moreover, you might like to take advantage of LBRY's in-app [rewards](https://lbry.io/faq/earn-credits).

Support
------
If you would like to support LBrRYn, you can send me `LBC` via `bTgYUMnmSufazfb1u2pRugDPminDmtNWE6`:

    LBRY w -s 1.0 bTgYUMnmSufazfb1u2pRugDPminDmtNWE6

Usage
----
Tips:
- One need not finish downloading a file to view it
- A requested download may not start immedidately; do not panic (and see next point)
- If something is not working as you think it ought, start the daemon with the --debug option to get the full output.  In ordinary use it rather gets in the way

        LBrRYn Usage
        LBRY [command] [options]
             [--help | -h]                           Print this help sheet
             [version | v]                           Print LBrRYn, and lbrynet versions
             [report_bug | rb]                       Report a bug
           Daemon
             [begin | b]                             Start the daemon 
                 -db | --debug                        verbosely
             [end | e]                               Stop the daemon
             [status | s]                            Check status of the daemon
           Content Consumption
             [subscribe | subs]                      Check current subscriptions
                 -a, --add [Channel]                  Add Channel to list of subscriptions
                 -u, --unsubscribe [Channel]          Remove channel from ... &c.
                 -l, --list                           List current subscriptions
             [channel_list| chls]                    List defaults 
                 [Channel]                            List [Channel] entries (default = Lunduke)
                 -p, --page [n]                       Show nth page (default = 1)
                 -ps, --page-size [n]                 Show n results per page (default = 10)
             [download | dl]  [[Channel]| ] [p1] [p2] [...]
                                                     Download particular video(s) from [default] Channel
                                                     p1, p2 are their positions in the current entry list
                                                     If any have are non-gratis, the user is prompted
             [file_list | fls]                       List all downloaded files
             [delete | del]                          Deletes...
                 -a, --all                            all downloaded files
                 [f1] [f2] [...]                      all files specified. 
                                                      f1, f2 are names as in [download_directory]
             [play | p] [f1] [f2] [...]              for x in "$@" $PLR [download_directory]/x & etc.
           Wallet Management
             [wallet | w]                            Access wallet operations: 
                 -b, --balance                        Show your wallet's balance
                 -l, --list                           List addresses associated with you wallet
                 -m, --mine [Address]                 Is [Address] yours?
                 -u, --unused                         Show an unused address, or generate one
                 -e, --encrypt                        Show message about backing up
                 -s, --send [Amount] [Address]        Send [Amount] LBC to [Address]
                            [Amount] [name]            Tip [name] with [Amount] LBC, where [name]
                                                       is a channel (e.g. @Lunduke), or filename as in fls
                 -r, --resolve [name]                 Get address for [name], e.g. to input into 
                                                      blockchain explorer.  Used in --send.
             [transaction_list | tls]                List transactions
             [transaction_show | tsh] [txid]         Show details of the [txid] transaction
           Settings Management
             [get_settings | gs]                     Show subset of settings
                 -a, --all                            or all
             [set_setting | ss]                      Set:
                 -dd, --download-dir [/path/to]       download directory to /path/to and move 
                                                      previously downloaded files. 
                                                       lbrynet-cli default: $USER/Downloads
                 -sh, --share-usage-data [y | n]      share_usage_data to true/false
                                                       lbrynet-cli default: true
                 -ml, --max-limit [y | n]             (en/dis)able maximum limit on download cost
                                                       lbrynet-cli default: y
                                  [USD | BTC | LBC] [value]
                                                      upper limit on cost of downloads in USD
                                                       lbrynet-cli default: US$50.00
 
Disclaimer
---------

\*Some names may be claimed as the property of others.
